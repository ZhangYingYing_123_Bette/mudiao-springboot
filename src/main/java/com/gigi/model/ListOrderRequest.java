package com.gigi.model;

import com.alibaba.cola.dto.PageQuery;
import lombok.Data;

@Data
public class ListOrderRequest extends PageQuery {

    private Long userId;

}
