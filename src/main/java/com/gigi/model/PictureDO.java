package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("图片实体")
@Data
public class PictureDO {

    private Long id;

    @NotBlank
    private String pic;

    @NotBlank
    private String title;

}
