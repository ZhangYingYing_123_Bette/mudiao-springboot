package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel("用户登录请求实体")
@Data
public class UserLoginRequest {

    @NotNull(message = "手机号不能为空")
    private Long phone;

    @NotBlank(message = "密码不能为空")
    private String pwd;

}
