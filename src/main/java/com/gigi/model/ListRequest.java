package com.gigi.model;

import com.alibaba.cola.dto.PageQuery;
import lombok.Data;

@Data
public class ListRequest extends PageQuery {
    private Integer ifByPage=1;
}
