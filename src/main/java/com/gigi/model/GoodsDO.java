package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel("商品实体")
@Data
public class GoodsDO {

    private Long id;

    @NotBlank
    private String pic;

    @NotBlank
    private String name;

    @NotNull
    private Float price;

    @NotBlank
    private String intro;

}
