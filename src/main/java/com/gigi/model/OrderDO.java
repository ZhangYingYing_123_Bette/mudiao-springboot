package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@ApiModel("订单实体")
public class OrderDO {

    @NotNull
    private Long userId;

    @NotNull
    private Long goodsId;

    @NotNull
    private Long addressId;

    @NotNull
    private Integer number;

    private Long id;

    private String userName;

    private String goodsName;

    private String goodsPic;

    private Float goodsPrice;

    private LocalDateTime time;

    private String timeStr;

    private Float total;

    private String receiver;

    private Long phone;

    private String address;

}
