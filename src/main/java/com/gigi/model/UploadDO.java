package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@ApiModel("上传文件实体")
@Data
public class UploadDO {
    @NotNull(message = "上传文件不能为空")
    private MultipartFile file;
}
