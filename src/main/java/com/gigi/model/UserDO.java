package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel("编辑用户资料实体")
@Data
public class UserDO {

    @NotNull(message = "主键id不能为空")
    private Long id;

    private String name;

    private String pic;

    private Float balance;

}
