package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("视频实体")
@Data
public class VideoDO {

    private Long id;

    @NotBlank
    private String video;

    @NotBlank
    private String intro;

    @NotBlank
    private String engIntro;

}
