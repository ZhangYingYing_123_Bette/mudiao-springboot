package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ApiModel("地址实体")
@Data
public class AddressDO {

    private Long id;

    @NotBlank
    private String receiver;

    @NotNull
    private Long deliveryPhone;

    @NotBlank
    private String address;

    @NotNull
    private Long userId;

}
