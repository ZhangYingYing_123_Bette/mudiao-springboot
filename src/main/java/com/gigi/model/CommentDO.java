package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@ApiModel("评论实体")
@Data
public class CommentDO {

    private Long id;

    @NotBlank
    private String content;

    private String timeStr;

    private String userPic;

    private String userName;

    private List<CommentDO> children;

    private String replyUserName;

    private String replyContent;

    @NotNull
    private Long userId;

    private Long parentId;

    private LocalDateTime time;

}
