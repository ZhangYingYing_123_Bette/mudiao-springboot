package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("传承人实体")
@Data
public class InheritorDO {

    private Long id;

    @NotBlank
    private String pic;

    @NotBlank
    private String name;

    @NotBlank
    private String title;

    @NotBlank
    private String intro;

}
