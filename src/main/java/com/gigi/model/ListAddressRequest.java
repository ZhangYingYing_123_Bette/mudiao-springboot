package com.gigi.model;

import com.alibaba.cola.dto.PageQuery;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ListAddressRequest extends PageQuery {

    @NotNull
    private Long userId;

}
