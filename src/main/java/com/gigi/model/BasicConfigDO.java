package com.gigi.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("基础配置实体")
@Data
public class BasicConfigDO {

    private Long id;

    @NotBlank
    private String infoPic;

    @NotBlank
    private String title;

    @NotBlank
    private String engTitle;

    @NotBlank
    private String secTitle;

    @NotBlank
    private String introPic;

    @NotBlank
    private String introDesc;

    @NotBlank
    private String introEngDesc;

}
