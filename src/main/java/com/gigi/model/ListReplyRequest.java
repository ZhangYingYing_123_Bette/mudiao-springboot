package com.gigi.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ListReplyRequest {

    @NotNull
    private Long commentId;

}
