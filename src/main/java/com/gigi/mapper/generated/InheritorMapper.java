package com.gigi.mapper.generated;

import com.gigi.entity.InheritorEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 传承人表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
public interface InheritorMapper extends BaseMapper<InheritorEntity> {

}
