package com.gigi.mapper.generated;

import com.gigi.entity.GoodsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
public interface GoodsMapper extends BaseMapper<GoodsEntity> {

}
