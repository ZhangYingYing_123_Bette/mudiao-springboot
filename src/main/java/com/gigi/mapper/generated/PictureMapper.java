package com.gigi.mapper.generated;

import com.gigi.entity.PictureEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 图片表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-12
 */
public interface PictureMapper extends BaseMapper<PictureEntity> {

}
