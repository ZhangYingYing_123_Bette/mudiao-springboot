package com.gigi.mapper.generated;

import com.gigi.entity.CommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论id Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
public interface CommentMapper extends BaseMapper<CommentEntity> {

}
