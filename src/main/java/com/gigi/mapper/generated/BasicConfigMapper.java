package com.gigi.mapper.generated;

import com.gigi.entity.BasicConfigEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 基础配置表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
public interface BasicConfigMapper extends BaseMapper<BasicConfigEntity> {

}
