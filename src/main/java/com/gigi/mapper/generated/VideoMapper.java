package com.gigi.mapper.generated;

import com.gigi.entity.VideoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 视频表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
public interface VideoMapper extends BaseMapper<VideoEntity> {

}
