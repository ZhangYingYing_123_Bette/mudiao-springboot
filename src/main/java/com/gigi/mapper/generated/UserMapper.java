package com.gigi.mapper.generated;

import com.gigi.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
public interface UserMapper extends BaseMapper<UserEntity> {

}
