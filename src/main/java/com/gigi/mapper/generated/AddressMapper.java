package com.gigi.mapper.generated;

import com.gigi.entity.AddressEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收货地址表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
public interface AddressMapper extends BaseMapper<AddressEntity> {

}
