package com.gigi.mapper.generated;

import com.gigi.entity.OrdersEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
public interface OrdersMapper extends BaseMapper<OrdersEntity> {

}
