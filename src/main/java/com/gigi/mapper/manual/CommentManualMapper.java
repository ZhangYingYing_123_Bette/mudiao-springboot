package com.gigi.mapper.manual;

import com.gigi.model.CommentDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CommentManualMapper {
    List<CommentDO> list(@Param("ifByPage") Integer ifByPage, @Param("startIndex") int startIndex, @Param("pageSize") int pageSize);
}
