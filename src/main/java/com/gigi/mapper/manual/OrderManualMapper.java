package com.gigi.mapper.manual;

import com.gigi.model.OrderDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderManualMapper {

    List<OrderDO> list(@Param("startIndex") int startIndex, @Param("pageSize") int pageSize, @Param("userId") Long userId);

    int count(@Param("userId") Long userId);

}
