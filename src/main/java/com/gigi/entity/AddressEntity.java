package com.gigi.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 收货地址表
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
@TableName("address")
@ApiModel(value="AddressEntity对象", description="收货地址表")
public class AddressEntity extends Model<AddressEntity> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "收货人")
    @TableField("receiver")
    private String receiver;

    @ApiModelProperty(value = "收货电话")
    @TableField("delivery_phone")
    private Long deliveryPhone;

    @ApiModelProperty(value = "收货地址")
    @TableField("address")
    private String address;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }
    public Long getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(Long deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "AddressEntity{" +
            "id=" + id +
            ", receiver=" + receiver +
            ", deliveryPhone=" + deliveryPhone +
            ", address=" + address +
            ", userId=" + userId +
        "}";
    }
}
