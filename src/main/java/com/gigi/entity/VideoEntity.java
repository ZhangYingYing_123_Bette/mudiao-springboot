package com.gigi.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 视频表
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
@TableName("video")
@ApiModel(value="VideoEntity对象", description="视频表")
public class VideoEntity extends Model<VideoEntity> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "视频")
    @TableField("video")
    private String video;

    @ApiModelProperty(value = "中文简介")
    @TableField("intro")
    private String intro;

    @ApiModelProperty(value = "英文简介")
    @TableField("eng_intro")
    private String engIntro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
    public String getEngIntro() {
        return engIntro;
    }

    public void setEngIntro(String engIntro) {
        this.engIntro = engIntro;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "VideoEntity{" +
            "id=" + id +
            ", video=" + video +
            ", intro=" + intro +
            ", engIntro=" + engIntro +
        "}";
    }
}
