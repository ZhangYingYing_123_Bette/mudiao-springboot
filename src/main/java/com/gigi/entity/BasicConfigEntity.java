package com.gigi.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 基础配置表
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
@TableName("basic_config")
@ApiModel(value="BasicConfigEntity对象", description="基础配置表")
public class BasicConfigEntity extends Model<BasicConfigEntity> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "首页背景图片")
    @TableField("info_pic")
    private String infoPic;

    @ApiModelProperty(value = "首页中文标题")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "首页英文标题")
    @TableField("eng_title")
    private String engTitle;

    @ApiModelProperty(value = "首页副标题")
    @TableField("sec_title")
    private String secTitle;

    @ApiModelProperty(value = "简介背景图片")
    @TableField("intro_pic")
    private String introPic;

    @ApiModelProperty(value = "简介描述")
    @TableField("intro_desc")
    private String introDesc;

    @ApiModelProperty(value = "简介英文描述")
    @TableField("intro_eng_desc")
    private String introEngDesc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getInfoPic() {
        return infoPic;
    }

    public void setInfoPic(String infoPic) {
        this.infoPic = infoPic;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getEngTitle() {
        return engTitle;
    }

    public void setEngTitle(String engTitle) {
        this.engTitle = engTitle;
    }
    public String getSecTitle() {
        return secTitle;
    }

    public void setSecTitle(String secTitle) {
        this.secTitle = secTitle;
    }
    public String getIntroPic() {
        return introPic;
    }

    public void setIntroPic(String introPic) {
        this.introPic = introPic;
    }
    public String getIntroDesc() {
        return introDesc;
    }

    public void setIntroDesc(String introDesc) {
        this.introDesc = introDesc;
    }
    public String getIntroEngDesc() {
        return introEngDesc;
    }

    public void setIntroEngDesc(String introEngDesc) {
        this.introEngDesc = introEngDesc;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BasicConfigEntity{" +
            "id=" + id +
            ", infoPic=" + infoPic +
            ", title=" + title +
            ", engTitle=" + engTitle +
            ", secTitle=" + secTitle +
            ", introPic=" + introPic +
            ", introDesc=" + introDesc +
            ", introEngDesc=" + introEngDesc +
        "}";
    }
}
