package com.gigi;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CodeGenerator {

    /**
     * 读取控制台内容
     *
     * @param tip
     * @return
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(tip + "：");
        return scanner.nextLine();
    }

    public static void main(String[] args) {
        String[] tables = scanner("请输入表名，多个英文逗号分割").split(",");
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // ------------------全局配置-------------------------------
        GlobalConfig gc = new GlobalConfig();
        // 工程根目录
        String projectPath = System.getProperty("user.dir");
        // 生成文件的输出目录
        gc.setOutputDir(projectPath + "/src/main/java/");
        // 作者
        gc.setAuthor("AutoGenerator");
        // 开启BaseResultMap 默认false
        gc.setBaseResultMap(true);
        // 开启BaseColumnList 默认false
        gc.setBaseColumnList(true);
        gc.setActiveRecord(true);
        // 命名方式
        gc.setServiceName("%sService");
        gc.setEntityName("%sEntity");
        gc.setServiceImplName("%sServiceImpl");
        gc.setMapperName("%sMapper");
        gc.setSwagger2(Boolean.TRUE);
        // 是否覆盖已有文件
        gc.setFileOverride(true);
        // 是否弹出文件夹窗口
        gc.setOpen(false);
        mpg.setGlobalConfig(gc);

        // -----------------数据源配置---------------------------
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/mudiao?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("12345678");
        mpg.setDataSource(dsc);

        // -------------------包配置--------------------------------
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.gigi");
        pc.setService("service.generated");
        pc.setServiceImpl("service.generated.impl");
        pc.setMapper("mapper.generated");
        pc.setEntity("entity");
        mpg.setPackageInfo(pc);

        // ---------------------自定义配置----------------------------
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名
                return projectPath + "/src/main/resources/mapper/generated/" + tableInfo.getMapperName() + StringPool.DOT_XML;
            }
        });
        focList.add(new FileOutConfig("/templates/service.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return projectPath + "/src/main/java/com/gigi/service/generated/" + tableInfo.getServiceName() + StringPool.DOT_JAVA;
            }
        });
        focList.add(new FileOutConfig("/templates/serviceImpl.java.ftl") {
            @Override
            public String outputFile(TableInfo tableInfo) {
                return projectPath + "/src/main/java/com/gigi/service/generated/impl/" + tableInfo.getServiceImplName() + StringPool.DOT_JAVA;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // -----------------------自定义配置模板------------------------------------------
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setService(null);
        templateConfig.setServiceImpl(null);
        templateConfig.setController(null);
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // -------------------------策略配置-------------------------------------------
        StrategyConfig strategy = new StrategyConfig();
        // 需要包含的表名，允许正则表达式（与exclude二选一配置）
        strategy.setInclude(tables);
        // 命名大写
        strategy.setCapitalMode(true);
        // 表名生成策略
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 数据库表字段映射到实体的命名策略，未指定按照naming执行
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setEntityTableFieldAnnotationEnable(true);
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }
}
