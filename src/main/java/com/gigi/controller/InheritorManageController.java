package com.gigi.controller;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.gigi.entity.InheritorEntity;
import com.gigi.model.IdRequest;
import com.gigi.model.InheritorDO;
import com.gigi.model.ListRequest;
import com.gigi.service.manual.InheritorManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 传承人管理
 */
@RestController
@RequestMapping("inheritor-manage")
public class InheritorManageController {

    private final InheritorManageService inheritorManageService;

    public InheritorManageController(InheritorManageService inheritorManageService) {
        this.inheritorManageService = inheritorManageService;
    }

    @ApiOperation(value = "列表")
    @PostMapping("/list")
    public MultiResponse<InheritorEntity> list(@RequestBody ListRequest request) {
        return inheritorManageService.list(request);
    }

    @ApiOperation(value = "详情")
    @PostMapping("/detail")
    public SingleResponse<InheritorEntity> detail(@RequestBody @Valid IdRequest request) {
        return inheritorManageService.detail(request.getId());
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Response save(@RequestBody @Valid InheritorDO request) {
        return inheritorManageService.save(request);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    public Response delete(@RequestBody @Valid IdRequest request) {
        return inheritorManageService.delete(request.getId());
    }

}
