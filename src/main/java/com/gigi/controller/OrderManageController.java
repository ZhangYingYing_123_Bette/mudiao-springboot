package com.gigi.controller;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.gigi.model.IdRequest;
import com.gigi.model.ListOrderRequest;
import com.gigi.model.OrderDO;
import com.gigi.service.manual.OrderManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 订单管理
 */
@RestController
@RequestMapping("order-manage")
public class OrderManageController {

    private final OrderManageService orderManageService;

    public OrderManageController(OrderManageService orderManageService) {
        this.orderManageService = orderManageService;
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Response save(@RequestBody @Valid OrderDO request) {
        return orderManageService.save(request);
    }

    @ApiOperation(value = "列表")
    @PostMapping("/list")
    public MultiResponse<OrderDO> list(@RequestBody ListOrderRequest request) {
        return orderManageService.list(request);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    public Response delete(@RequestBody @Valid IdRequest request) {
        return orderManageService.delete(request.getId());
    }

}
