package com.gigi.controller;

import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.gigi.entity.BasicConfigEntity;
import com.gigi.model.BasicConfigDO;
import com.gigi.service.manual.BasicConfigManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 基础配置管理
 */
@RestController
@RequestMapping("basic-manage")
public class BasicConfigManageController {

    private final BasicConfigManageService basicConfigManageService;

    public BasicConfigManageController(BasicConfigManageService basicConfigManageService) {
        this.basicConfigManageService = basicConfigManageService;
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Response save(@RequestBody @Valid BasicConfigDO request) {
        return basicConfigManageService.save(request);
    }

    @ApiOperation(value = "详情")
    @PostMapping("/detail")
    public SingleResponse<BasicConfigEntity> detail() {
        return basicConfigManageService.detail();
    }

}
