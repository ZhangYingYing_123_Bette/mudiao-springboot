package com.gigi.controller;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.gigi.entity.GoodsEntity;
import com.gigi.model.GoodsDO;
import com.gigi.model.IdRequest;
import com.gigi.model.ListRequest;
import com.gigi.service.manual.GoodsManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 商品管理
 */
@RestController
@RequestMapping("goods-manage")
public class GoodsManageController {

    private final GoodsManageService goodsManageService;

    public GoodsManageController(GoodsManageService goodsManageService) {
        this.goodsManageService = goodsManageService;
    }

    @ApiOperation(value = "列表")
    @PostMapping("/list")
    public MultiResponse<GoodsEntity> list(@RequestBody ListRequest request) {
        return goodsManageService.list(request);
    }

    @ApiOperation(value = "详情")
    @PostMapping("/detail")
    public SingleResponse<GoodsEntity> detail(@RequestBody @Valid IdRequest request) {
        return goodsManageService.detail(request.getId());
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Response save(@RequestBody @Valid GoodsDO request) {
        return goodsManageService.save(request);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    public Response delete(@RequestBody @Valid IdRequest request) {
        return goodsManageService.delete(request.getId());
    }

}
