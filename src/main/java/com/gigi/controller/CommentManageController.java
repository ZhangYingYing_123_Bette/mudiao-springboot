package com.gigi.controller;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.gigi.model.CommentDO;
import com.gigi.model.IdRequest;
import com.gigi.model.ListRequest;
import com.gigi.service.manual.CommentManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 评论管理
 */
@RestController
@RequestMapping("comment-manage")
public class CommentManageController {

    private final CommentManageService commentManageService;

    public CommentManageController(CommentManageService commentManageService) {
        this.commentManageService = commentManageService;
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Response save(@RequestBody @Valid CommentDO request) {
        return commentManageService.save(request);
    }

    @ApiOperation(value = "列表")
    @PostMapping("/list")
    public MultiResponse<CommentDO> list(@RequestBody @Valid ListRequest request) {
        return commentManageService.list(request);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    public Response delete(@RequestBody @Valid IdRequest request) {
        return commentManageService.delete(request.getId());
    }

}
