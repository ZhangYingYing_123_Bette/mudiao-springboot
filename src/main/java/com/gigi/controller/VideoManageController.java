package com.gigi.controller;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.gigi.entity.VideoEntity;
import com.gigi.model.IdRequest;
import com.gigi.model.ListRequest;
import com.gigi.model.VideoDO;
import com.gigi.service.manual.VideoManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 视频管理
 */
@RestController
@RequestMapping("video-manage")
public class VideoManageController {

    private final VideoManageService videoManageService;

    public VideoManageController(VideoManageService videoManageService) {
        this.videoManageService = videoManageService;
    }

    @ApiOperation(value = "列表")
    @PostMapping("/list")
    public MultiResponse<VideoEntity> list(@RequestBody ListRequest request) {
        return videoManageService.list(request);
    }

    @ApiOperation(value = "详情")
    @PostMapping("/detail")
    public SingleResponse<VideoEntity> detail(@RequestBody @Valid IdRequest request) {
        return videoManageService.detail(request.getId());
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Response save(@RequestBody @Valid VideoDO request) {
        return videoManageService.save(request);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    public Response delete(@RequestBody @Valid IdRequest request) {
        return videoManageService.delete(request.getId());
    }

}
