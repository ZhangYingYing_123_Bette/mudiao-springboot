package com.gigi.controller;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.gigi.entity.AddressEntity;
import com.gigi.model.AddressDO;
import com.gigi.model.IdRequest;
import com.gigi.model.ListAddressRequest;
import com.gigi.service.manual.AddressManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 地址管理
 */
@RestController
@RequestMapping("address-manage")
public class AddressManageController {

    private final AddressManageService addressManageService;

    public AddressManageController(AddressManageService addressManageService) {
        this.addressManageService = addressManageService;
    }

    @ApiOperation(value = "列表")
    @PostMapping("/list")
    public MultiResponse<AddressEntity> list(@RequestBody ListAddressRequest request) {
        return addressManageService.list(request);
    }

    @ApiOperation(value = "详情")
    @PostMapping("/detail")
    public SingleResponse<AddressEntity> detail(@RequestBody @Valid IdRequest request) {
        return addressManageService.detail(request.getId());
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Response save(@RequestBody @Valid AddressDO request) {
        return addressManageService.save(request);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    public Response delete(@RequestBody @Valid IdRequest request) {
        return addressManageService.delete(request.getId());
    }

}
