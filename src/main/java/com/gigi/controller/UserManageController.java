package com.gigi.controller;

import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.gigi.entity.UserEntity;
import com.gigi.model.IdRequest;
import com.gigi.model.UserDO;
import com.gigi.model.UserLoginRequest;
import com.gigi.service.manual.UserManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 用户管理
 */
@RestController
@RequestMapping("user-manage")
public class UserManageController {

    private final UserManageService UserManageService;

    public UserManageController(UserManageService UserManageService) {
        this.UserManageService = UserManageService;
    }

    @ApiOperation(value = "登录")
    @PostMapping("/login")
    public SingleResponse<Long> login(@RequestBody @Valid UserLoginRequest request) {
        return UserManageService.login(request);
    }

    @ApiOperation(value = "详情")
    @PostMapping("/detail")
    public SingleResponse<UserEntity> detail(@RequestBody @Valid IdRequest request) {
        return UserManageService.detail(request);
    }

    @ApiOperation(value = "注册")
    @PostMapping("/register")
    public Response register(@RequestBody @Valid UserLoginRequest request) {
        return UserManageService.register(request);
    }

    @ApiOperation(value = "编辑资料")
    @PostMapping("/edit")
    public Response edit(@RequestBody @Valid UserDO request) {
        return UserManageService.edit(request);
    }

}
