package com.gigi.controller;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.gigi.entity.PictureEntity;
import com.gigi.model.IdRequest;
import com.gigi.model.ListRequest;
import com.gigi.model.PictureDO;
import com.gigi.service.manual.PictureManageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 图片管理
 */
@RestController
@RequestMapping("picture-manage")
public class PictureManageController {

    private final PictureManageService pictureManageService;

    public PictureManageController(PictureManageService pictureManageService) {
        this.pictureManageService = pictureManageService;
    }

    @ApiOperation(value = "列表")
    @PostMapping("/list")
    public MultiResponse<PictureEntity> list(@RequestBody ListRequest request) {
        return pictureManageService.list(request);
    }

    @ApiOperation(value = "详情")
    @PostMapping("/detail")
    public SingleResponse<PictureEntity> detail(@RequestBody @Valid IdRequest request) {
        return pictureManageService.detail(request.getId());
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Response save(@RequestBody @Valid PictureDO request) {
        return pictureManageService.save(request);
    }

    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    public Response delete(@RequestBody @Valid IdRequest request) {
        return pictureManageService.delete(request.getId());
    }

}
