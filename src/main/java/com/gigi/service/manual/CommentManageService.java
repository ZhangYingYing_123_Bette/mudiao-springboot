package com.gigi.service.manual;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.gigi.entity.CommentEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.enums.YesOrNoEnum;
import com.gigi.mapper.generated.CommentMapper;
import com.gigi.mapper.manual.CommentManualMapper;
import com.gigi.model.CommentDO;
import com.gigi.model.ListRequest;
import com.gigi.service.generated.CommentService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CommentManageService {

    private final CommentService commentService;
    private final CommentManualMapper commentManualMapper;
    private final CommentMapper commentMapper;

    public CommentManageService(CommentService commentService, CommentManualMapper commentManualMapper, CommentMapper commentMapper) {
        this.commentService = commentService;
        this.commentManualMapper = commentManualMapper;
        this.commentMapper = commentMapper;
    }

    /**
     * 保存接口
     *
     * @param request
     * @return
     */
    public Response save(CommentDO request) {
        CommentEntity target = new CommentEntity();
        target.setContent(request.getContent());
        target.setTime(LocalDateTime.now());
        target.setUserId(request.getUserId());
        target.setReplyUserName(request.getReplyUserName());
        target.setReplyContent(request.getReplyContent());
        target.setParentId(request.getParentId());
        boolean isSuccess = commentService.save(target);
        if (!isSuccess) {
            return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(), ErrorCodeEnum.SAVE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

    /**
     * 列表接口
     *
     * @return
     */
    public MultiResponse<CommentDO> list(ListRequest request) {
        if (request.getPageNum() < 1) {
            request.setPageNum(1);
        }
        if (request.getPageSize() < 1) {
            request.setPageSize(10);
        }

        int startIndex = (request.getPageNum() - 1) * request.getPageSize();

        List<CommentDO> list = commentManualMapper.list(request.getIfByPage(), startIndex, request.getPageSize());
        if (CollUtil.isEmpty(list)) {
            return MultiResponse.of(Collections.emptyList(), 0);
        }
        List<CommentDO> commentDOS = list.stream().map(e -> {
            CommentDO commentDO = new CommentDO();
            commentDO.setId(e.getId());
            commentDO.setContent(e.getContent());
            commentDO.setUserId(e.getUserId());
            commentDO.setUserName(e.getUserName());
            commentDO.setUserPic(e.getUserPic());
            commentDO.setReplyUserName(e.getReplyUserName());
            commentDO.setReplyContent(e.getReplyContent());
            commentDO.setParentId(e.getParentId());
            commentDO.setTimeStr(DateUtil.format(e.getTime(), DatePattern.NORM_DATETIME_PATTERN));
            return commentDO;
        }).collect(Collectors.toList());

        if (!YesOrNoEnum.YES.getCode().equals(request.getIfByPage())) {
            List<CommentDO> rootList = commentDOS.stream().filter(e -> e.getParentId() == 0).collect(Collectors.toList());
            rootList.forEach(e -> {
                e.setChildren(commentDOS.stream().filter(i -> e.getId().equals(i.getParentId())).collect(Collectors.toList()));
            });
            return MultiResponse.ofWithoutTotal(rootList);
        }
        return MultiResponse.of(commentDOS, commentService.count());
    }

    /**
     * 删除接口
     *
     * @param id
     * @return
     */
    public Response delete(Long id) {
        CommentEntity entity = commentMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return Response.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        if (!commentService.removeById(id)) {
            return Response.buildFailure(ErrorCodeEnum.DELETE_FAILED.getErrCode(), ErrorCodeEnum.DELETE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

}
