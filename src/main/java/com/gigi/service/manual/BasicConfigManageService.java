package com.gigi.service.manual;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gigi.entity.BasicConfigEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.model.BasicConfigDO;
import com.gigi.service.generated.BasicConfigService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class BasicConfigManageService {

    private final BasicConfigService basicConfigService;

    public BasicConfigManageService(BasicConfigService basicConfigService) {
        this.basicConfigService = basicConfigService;
    }

    /**
     * 保存接口
     *
     * @param request
     * @return
     */
    public Response save(BasicConfigDO request) {
        if (ObjectUtil.isNull(request.getId())) {
            BasicConfigEntity target = buildTarget(request);
            boolean isSuccess = basicConfigService.save(target);
            if (!isSuccess) {
                return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(), ErrorCodeEnum.SAVE_FAILED.getErrDesc());
            }
        } else {
            BasicConfigEntity entity = basicConfigService.getById(request.getId());
            if (Objects.isNull(entity)) {
                return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
            }
            BasicConfigEntity target = buildTarget(request);
            target.setId(entity.getId());
            boolean isSuccess = basicConfigService.updateById(target);
            if (!isSuccess) {
                return Response.buildFailure(ErrorCodeEnum.EDIT_FAILED.getErrCode(), ErrorCodeEnum.EDIT_FAILED.getErrDesc());
            }
        }
        return Response.buildSuccess();
    }

    /**
     * 创建target对象公共方法
     *
     * @param request
     * @return
     */
    private BasicConfigEntity buildTarget(BasicConfigDO request) {
        BasicConfigEntity target = new BasicConfigEntity();
        target.setInfoPic(request.getInfoPic());
        target.setTitle(request.getTitle());
        target.setEngTitle(request.getEngTitle());
        target.setSecTitle(request.getSecTitle());
        target.setIntroPic(request.getIntroPic());
        target.setIntroDesc(request.getIntroDesc());
        target.setIntroEngDesc(request.getIntroEngDesc());
        return target;
    }

    /**
     * 详情接口
     *
     * @param id
     * @return
     */
    public SingleResponse<BasicConfigEntity> detail() {
        LambdaQueryWrapper<BasicConfigEntity> wrapper = new LambdaQueryWrapper<BasicConfigEntity>()
                .orderByAsc(BasicConfigEntity::getId);
        List<BasicConfigEntity> configEntities = basicConfigService.list(wrapper);
        if (CollUtil.isEmpty(configEntities)) {
            return SingleResponse.of(new BasicConfigEntity());
        }
        return SingleResponse.of(configEntities.get(0));
    }

}
