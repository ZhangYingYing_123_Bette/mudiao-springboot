package com.gigi.service.manual;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gigi.entity.InheritorEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.mapper.generated.InheritorMapper;
import com.gigi.model.InheritorDO;
import com.gigi.model.ListRequest;
import com.gigi.service.generated.InheritorService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class InheritorManageService {

    private final InheritorMapper inheritorMapper;
    private final InheritorService inheritorService;

    public InheritorManageService(InheritorMapper inheritorMapper, InheritorService inheritorService) {
        this.inheritorMapper = inheritorMapper;
        this.inheritorService = inheritorService;
    }

    /**
     * 列表接口
     *
     * @param request
     * @return
     */
    public MultiResponse<InheritorEntity> list(ListRequest request) {
        if (request.getPageNum() < 1) {
            request.setPageNum(1);
        }
        if (request.getPageSize() < 1) {
            request.setPageSize(10);
        }

        LambdaQueryWrapper<InheritorEntity> wrapper = new LambdaQueryWrapper<InheritorEntity>().orderByDesc(InheritorEntity::getId);
        Page<InheritorEntity> page = new Page<>(request.getPageNum(), request.getPageSize());
        Page<InheritorEntity> inheritorEntityPage = inheritorMapper.selectPage(page, wrapper);

        return MultiResponse.of(inheritorEntityPage.getRecords(), (int) inheritorEntityPage.getTotal());
    }

    /**
     * 详情接口
     *
     * @param id
     * @return
     */
    public SingleResponse<InheritorEntity> detail(Long id) {
        InheritorEntity entity = inheritorMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        return SingleResponse.of(entity);
    }

    /**
     * 保存接口
     *
     * @param request
     * @return
     */
    public Response save(InheritorDO request) {
        if (ObjectUtil.isNull(request.getId())) {
            InheritorEntity target = buildTarget(request);
            boolean isSuccess = inheritorService.save(target);
            if (!isSuccess) {
                return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(), ErrorCodeEnum.SAVE_FAILED.getErrDesc());
            }
        } else {
            InheritorEntity entity = inheritorMapper.selectById(request.getId());
            if (Objects.isNull(entity)) {
                return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
            }
            InheritorEntity target = buildTarget(request);
            target.setId(entity.getId());
            boolean isSuccess = inheritorService.updateById(target);
            if (!isSuccess) {
                return Response.buildFailure(ErrorCodeEnum.EDIT_FAILED.getErrCode(), ErrorCodeEnum.EDIT_FAILED.getErrDesc());
            }
        }
        return Response.buildSuccess();
    }

    /**
     * 创建target对象公共方法
     *
     * @param request
     * @return
     */
    private InheritorEntity buildTarget(InheritorDO request) {
        InheritorEntity target = new InheritorEntity();
        target.setPic(request.getPic());
        target.setTitle(request.getTitle());
        target.setName(request.getName());
        target.setIntro(request.getIntro());
        return target;
    }

    /**
     * 删除接口
     *
     * @param id
     * @return
     */
    public Response delete(Long id) {
        InheritorEntity entity = inheritorMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return Response.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        if (!inheritorService.removeById(id)) {
            return Response.buildFailure(ErrorCodeEnum.DELETE_FAILED.getErrCode(), ErrorCodeEnum.DELETE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

}
