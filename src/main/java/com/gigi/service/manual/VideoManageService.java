package com.gigi.service.manual;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gigi.entity.VideoEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.mapper.generated.VideoMapper;
import com.gigi.model.ListRequest;
import com.gigi.model.VideoDO;
import com.gigi.service.generated.VideoService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class VideoManageService {

    private final VideoService videoService;
    private final VideoMapper videoMapper;

    public VideoManageService(VideoService videoService, VideoMapper videoMapper) {
        this.videoService = videoService;
        this.videoMapper = videoMapper;
    }

    /**
     * 列表接口
     *
     * @param request
     * @return
     */
    public MultiResponse<VideoEntity> list(ListRequest request) {
        if (request.getPageNum() < 1) {
            request.setPageNum(1);
        }
        if (request.getPageSize() < 1) {
            request.setPageSize(10);
        }

        LambdaQueryWrapper<VideoEntity> wrapper = new LambdaQueryWrapper<VideoEntity>().orderByDesc(VideoEntity::getId);
        Page<VideoEntity> page = new Page<>(request.getPageNum(), request.getPageSize());
        Page<VideoEntity> videoEntityPage = videoMapper.selectPage(page, wrapper);

        return MultiResponse.of(videoEntityPage.getRecords(), (int) videoEntityPage.getTotal());
    }

    /**
     * 详情接口
     *
     * @param id
     * @return
     */
    public SingleResponse<VideoEntity> detail(Long id) {
        VideoEntity entity = videoMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        return SingleResponse.of(entity);
    }

    /**
     * 保存接口
     *
     * @param request
     * @return
     */
    public Response save(VideoDO request) {
        if (ObjectUtil.isNull(request.getId())) {
            VideoEntity target = buildTarget(request);
            boolean isSuccess = videoService.save(target);
            if (!isSuccess) {
                return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(), ErrorCodeEnum.SAVE_FAILED.getErrDesc());
            }
        } else {
            VideoEntity entity = videoMapper.selectById(request.getId());
            if (Objects.isNull(entity)) {
                return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
            }
            VideoEntity target = buildTarget(request);
            target.setId(entity.getId());
            boolean isSuccess = videoService.updateById(target);
            if (!isSuccess) {
                return Response.buildFailure(ErrorCodeEnum.EDIT_FAILED.getErrCode(), ErrorCodeEnum.EDIT_FAILED.getErrDesc());
            }
        }
        return Response.buildSuccess();
    }

    /**
     * 创建target对象公共方法
     *
     * @param request
     * @return
     */
    private VideoEntity buildTarget(VideoDO request) {
        VideoEntity target = new VideoEntity();
        target.setVideo(request.getVideo());
        target.setIntro(request.getIntro());
        target.setEngIntro(request.getEngIntro());
        return target;
    }

    /**
     * 删除接口
     *
     * @param id
     * @return
     */
    public Response delete(Long id) {
        VideoEntity entity = videoMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return Response.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        if (!videoService.removeById(id)) {
            return Response.buildFailure(ErrorCodeEnum.DELETE_FAILED.getErrCode(), ErrorCodeEnum.DELETE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

}
