package com.gigi.service.manual;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.gigi.entity.GoodsEntity;
import com.gigi.entity.OrdersEntity;
import com.gigi.entity.UserEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.mapper.generated.OrdersMapper;
import com.gigi.mapper.manual.OrderManualMapper;
import com.gigi.model.ListOrderRequest;
import com.gigi.model.OrderDO;
import com.gigi.service.generated.GoodsService;
import com.gigi.service.generated.OrdersService;
import com.gigi.service.generated.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class OrderManageService {

    private final OrdersService ordersService;
    private final OrdersMapper ordersMapper;
    private final UserService userService;
    private final GoodsService goodsService;
    private final OrderManualMapper orderManualMapper;

    public OrderManageService(OrdersService ordersService, OrdersMapper ordersMapper, UserService userService, GoodsService goodsService,
                              OrderManualMapper orderManualMapper) {
        this.ordersService = ordersService;
        this.ordersMapper = ordersMapper;
        this.userService = userService;
        this.goodsService = goodsService;
        this.orderManualMapper = orderManualMapper;
    }

    /**
     * 保存接口
     *
     * @param request
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Response save(OrderDO request) {

        // 存订单
        OrdersEntity entity = new OrdersEntity();
        entity.setUserId(request.getUserId());
        entity.setGoodsId(request.getGoodsId());
        entity.setAddressId(request.getAddressId());
        entity.setNumber(request.getNumber());
        entity.setTime(LocalDateTime.now());
        if (!ordersService.save(entity)) {
            return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(), ErrorCodeEnum.SAVE_FAILED.getErrDesc());
        }

        // 改余额
        GoodsEntity goods = goodsService.getById(request.getGoodsId());
        if (Objects.isNull(goods)) {
            return Response.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }

        UserEntity user = userService.getById(request.getUserId());
        if (Objects.isNull(user)) {
            return Response.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }

        UserEntity userEntity = new UserEntity();
        userEntity.setId(request.getUserId());
        userEntity.setBalance(user.getBalance() - goods.getPrice() * request.getNumber());
        if (!userService.updateById(userEntity)) {
            return Response.buildFailure(ErrorCodeEnum.EDIT_FAILED.getErrCode(), ErrorCodeEnum.EDIT_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

    /**
     * 列表接口
     *
     * @return
     */
    public MultiResponse<OrderDO> list(ListOrderRequest request) {
        if (request.getPageNum() < 1) {
            request.setPageNum(1);
        }
        if (request.getPageSize() < 1) {
            request.setPageSize(10);
        }

        int startIndex = (request.getPageNum() - 1) * request.getPageSize();

        List<OrderDO> list = orderManualMapper.list(startIndex, request.getPageSize(), request.getUserId());
        if (CollUtil.isEmpty(list)) {
            return MultiResponse.of(Collections.emptyList(), 0);
        }

        list.forEach(e -> {
            e.setTimeStr(DateUtil.format(e.getTime(), DatePattern.NORM_DATETIME_PATTERN));
            e.setTotal(e.getGoodsPrice() * e.getNumber());
        });

        return MultiResponse.of(list, orderManualMapper.count(request.getUserId()));
    }

    /**
     * 删除接口
     *
     * @param id
     * @return
     */
    public Response delete(Long id) {
        if (!ordersService.removeById(id)) {
            return Response.buildFailure(ErrorCodeEnum.DELETE_FAILED.getErrCode(), ErrorCodeEnum.DELETE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

}
