package com.gigi.service.manual;

import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.gigi.entity.UserEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.mapper.generated.UserMapper;
import com.gigi.model.IdRequest;
import com.gigi.model.UserDO;
import com.gigi.model.UserLoginRequest;
import com.gigi.service.generated.UserService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Objects;

@Service
public class UserManageService {

    private final UserService userService;
    private final UserMapper userMapper;

    public UserManageService(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    /**
     * 登录接口
     *
     * @param request
     * @return
     */
    public SingleResponse<Long> login(UserLoginRequest request) {
        LambdaQueryWrapper<UserEntity> wrapper = new LambdaQueryWrapper<UserEntity>()
                .eq(UserEntity::getPhone, request.getPhone())
                .eq(UserEntity::getPassword, request.getPwd());
        UserEntity entity = userService.getOne(wrapper, false);
        if (Objects.isNull(entity)) {
            return SingleResponse.buildFailure(ErrorCodeEnum.ACCOUNT_NOT_EXIST.getErrCode(),
                    ErrorCodeEnum.ACCOUNT_NOT_EXIST.getErrDesc());
        }
        return SingleResponse.of(entity.getId());
    }

    /**
     * 详情接口
     *
     * @param request
     * @return
     */
    public SingleResponse<UserEntity> detail(IdRequest request) {
        UserEntity user = userService.getById(request.getId());
        if (Objects.isNull(user)) {
            return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(),
                    ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        return SingleResponse.of(user);
    }

    /**
     * 注册接口
     *
     * @param request
     * @return
     */
    public Response register(@RequestBody @Valid UserLoginRequest request) {
        LambdaQueryWrapper<UserEntity> wrapper = new LambdaQueryWrapper<UserEntity>()
                .eq(UserEntity::getPhone, request.getPhone());
        if (userService.count(wrapper) > 0) {
            return Response.buildFailure(ErrorCodeEnum.ALREADY_REGISTER.getErrCode(),
                    ErrorCodeEnum.ALREADY_REGISTER.getErrDesc());
        }
        UserEntity entity = new UserEntity();
        entity.setPhone(request.getPhone());
        entity.setPassword(request.getPwd());
        entity.setName("新用户");
        entity.setPic("/static/default.jpeg");
        if (!userService.save(entity)) {
            return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(),
                    ErrorCodeEnum.SAVE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

    /**
     * 编辑资料接口
     *
     * @param request
     * @return
     */
    public Response edit(UserDO request) {
        UserEntity entity = userMapper.selectById(request.getId());
        if (Objects.isNull(entity)) {
            return Response.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        UserEntity target = new UserEntity();
        target.setId(entity.getId());
        target.setPic(request.getPic());
        target.setName(request.getName());
        target.setBalance(request.getBalance());
        boolean isSuccess = userService.updateById(target);
        if (!isSuccess) {
            return Response.buildFailure(ErrorCodeEnum.EDIT_FAILED.getErrCode(), ErrorCodeEnum.EDIT_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

}
