package com.gigi.service.manual;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gigi.entity.PictureEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.mapper.generated.PictureMapper;
import com.gigi.model.ListRequest;
import com.gigi.model.PictureDO;
import com.gigi.service.generated.PictureService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class PictureManageService {

    private final PictureMapper pictureMapper;
    private final PictureService pictureService;

    public PictureManageService(PictureMapper pictureMapper, PictureService pictureService) {
        this.pictureMapper = pictureMapper;
        this.pictureService = pictureService;
    }

    /**
     * 列表接口
     *
     * @param request
     * @return
     */
    public MultiResponse<PictureEntity> list(ListRequest request) {
        if (request.getPageNum() < 1) {
            request.setPageNum(1);
        }
        if (request.getPageSize() < 1) {
            request.setPageSize(10);
        }

        LambdaQueryWrapper<PictureEntity> wrapper = new LambdaQueryWrapper<PictureEntity>()
                .orderByDesc(PictureEntity::getId);
        Page<PictureEntity> page = new Page<>(request.getPageNum(), request.getPageSize());
        Page<PictureEntity> pictureEntityPage = pictureMapper.selectPage(page, wrapper);

        return MultiResponse.of(pictureEntityPage.getRecords(), (int) pictureEntityPage.getTotal());
    }

    /**
     * 详情接口
     *
     * @param id
     * @return
     */
    public SingleResponse<PictureEntity> detail(Long id) {
        PictureEntity entity = pictureMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        return SingleResponse.of(entity);
    }

    /**
     * 保存接口
     *
     * @param request
     * @return
     */
    public Response save(PictureDO request) {
        if (ObjectUtil.isNull(request.getId())) {
            PictureEntity target = buildTarget(request);
            boolean isSuccess = pictureService.save(target);
            if (!isSuccess) {
                return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(), ErrorCodeEnum.SAVE_FAILED.getErrDesc());
            }
        } else {
            PictureEntity entity = pictureMapper.selectById(request.getId());
            if (Objects.isNull(entity)) {
                return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
            }
            PictureEntity target = buildTarget(request);
            target.setId(entity.getId());
            boolean isSuccess = pictureService.updateById(target);
            if (!isSuccess) {
                return Response.buildFailure(ErrorCodeEnum.EDIT_FAILED.getErrCode(), ErrorCodeEnum.EDIT_FAILED.getErrDesc());
            }
        }
        return Response.buildSuccess();
    }

    /**
     * 创建target对象公共方法
     *
     * @param request
     * @return
     */
    private PictureEntity buildTarget(PictureDO request) {
        PictureEntity target = new PictureEntity();
        target.setPic(request.getPic());
        target.setTitle(request.getTitle());
        return target;
    }

    /**
     * 删除接口
     *
     * @param id
     * @return
     */
    public Response delete(Long id) {
        PictureEntity entity = pictureMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return Response.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        if (!pictureService.removeById(id)) {
            return Response.buildFailure(ErrorCodeEnum.DELETE_FAILED.getErrCode(), ErrorCodeEnum.DELETE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

}
