package com.gigi.service.manual;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gigi.entity.GoodsEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.mapper.generated.GoodsMapper;
import com.gigi.model.GoodsDO;
import com.gigi.model.ListRequest;
import com.gigi.service.generated.GoodsService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class GoodsManageService {

    private final GoodsService goodsService;
    private final GoodsMapper goodsMapper;

    public GoodsManageService(GoodsService goodsService, GoodsMapper goodsMapper) {
        this.goodsService = goodsService;
        this.goodsMapper = goodsMapper;
    }

    /**
     * 列表接口
     *
     * @param request
     * @return
     */
    public MultiResponse<GoodsEntity> list(ListRequest request) {
        if (request.getPageNum() < 1) {
            request.setPageNum(1);
        }
        if (request.getPageSize() < 1) {
            request.setPageSize(10);
        }

        LambdaQueryWrapper<GoodsEntity> wrapper = new LambdaQueryWrapper<GoodsEntity>().orderByDesc(GoodsEntity::getId);
        Page<GoodsEntity> page = new Page<>(request.getPageNum(), request.getPageSize());
        Page<GoodsEntity> goodsEntityPage = goodsMapper.selectPage(page, wrapper);

        return MultiResponse.of(goodsEntityPage.getRecords(), (int) goodsEntityPage.getTotal());
    }

    /**
     * 详情接口
     *
     * @param id
     * @return
     */
    public SingleResponse<GoodsEntity> detail(Long id) {
        GoodsEntity entity = goodsMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        return SingleResponse.of(entity);
    }

    /**
     * 保存接口
     *
     * @param request
     * @return
     */
    public Response save(GoodsDO request) {
        if (ObjectUtil.isNull(request.getId())) {
            GoodsEntity target = buildTarget(request);
            if (!goodsService.save(target)) {
                return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(), ErrorCodeEnum.SAVE_FAILED.getErrDesc());
            }
        } else {
            GoodsEntity target = buildTarget(request);
            target.setId(request.getId());
            if (!goodsService.updateById(target)) {
                return Response.buildFailure(ErrorCodeEnum.EDIT_FAILED.getErrCode(), ErrorCodeEnum.EDIT_FAILED.getErrDesc());
            }
        }
        return Response.buildSuccess();
    }

    /**
     * 创建target对象公共方法
     *
     * @param request
     * @return
     */
    private GoodsEntity buildTarget(GoodsDO request) {
        GoodsEntity target = new GoodsEntity();
        target.setPic(request.getPic());
        target.setPrice(request.getPrice());
        target.setName(request.getName());
        target.setIntro(request.getIntro());
        return target;
    }

    /**
     * 删除接口
     *
     * @param id
     * @return
     */
    public Response delete(Long id) {
        if (!goodsService.removeById(id)) {
            return Response.buildFailure(ErrorCodeEnum.DELETE_FAILED.getErrCode(), ErrorCodeEnum.DELETE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

}
