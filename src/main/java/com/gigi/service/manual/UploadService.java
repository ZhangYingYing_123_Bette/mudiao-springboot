package com.gigi.service.manual;

import com.alibaba.cola.dto.SingleResponse;
import com.gigi.enums.ErrorCodeEnum;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

@Service
public class UploadService {

    /**
     * 上传图片至 /static文件夹
     *
     * @param file
     * @return 存储到数据库的图片地址
     */
    public SingleResponse<String> upload(MultipartFile file) {
        if (Objects.isNull(file)) {
            return SingleResponse.buildFailure(ErrorCodeEnum.FILE_EMPTY.getErrCode(),
                    ErrorCodeEnum.FILE_EMPTY.getErrDesc());
        }
        // 当前时间毫秒数+图片名称
        String fileName = System.currentTimeMillis() + file.getOriginalFilename();
        // 文件夹路径
        String filePath = System.getProperty("user.dir") + System.getProperty(
                "file.separator") + "static";
        // 文件夹不存在新建
        File file1 = new File(filePath);
        if (!file1.exists()) {
            file1.mkdir();
        }
        // 图片路径
        File file2 = new File(filePath + System.getProperty("file.separator") + fileName);
        // 存图片至文件夹
        try {
            file.transferTo(file2);
        } catch (IOException e) {
            e.printStackTrace();
            return SingleResponse.buildFailure(ErrorCodeEnum.UPLOAD_FILE_FAILED.getErrCode(),
                    ErrorCodeEnum.UPLOAD_FILE_FAILED.getErrDesc());
        }
        // 存储到数据库的图片相对路径
        String path = "/static/" + fileName;
        return SingleResponse.of(path);
    }


}
