package com.gigi.service.manual;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.alibaba.cola.dto.SingleResponse;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gigi.entity.AddressEntity;
import com.gigi.enums.ErrorCodeEnum;
import com.gigi.mapper.generated.AddressMapper;
import com.gigi.model.AddressDO;
import com.gigi.model.ListAddressRequest;
import com.gigi.service.generated.AddressService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class AddressManageService {

    private final AddressService addressService;
    private final AddressMapper addressMapper;

    public AddressManageService(AddressService addressService, AddressMapper addressMapper) {
        this.addressService = addressService;
        this.addressMapper = addressMapper;
    }

    /**
     * 列表接口
     *
     * @param request
     * @return
     */
    public MultiResponse<AddressEntity> list(ListAddressRequest request) {
        if (request.getPageNum() < 1) {
            request.setPageNum(1);
        }
        if (request.getPageSize() < 1) {
            request.setPageSize(10);
        }

        LambdaQueryWrapper<AddressEntity> wrapper = new LambdaQueryWrapper<AddressEntity>()
                .eq(AddressEntity::getUserId, request.getUserId())
                .orderByDesc(AddressEntity::getId);
        Page<AddressEntity> page = new Page<>(request.getPageNum(), request.getPageSize());
        Page<AddressEntity> addressEntityPage = addressMapper.selectPage(page, wrapper);

        return MultiResponse.of(addressEntityPage.getRecords(), (int) addressEntityPage.getTotal());
    }

    /**
     * 详情接口
     *
     * @param id
     * @return
     */
    public SingleResponse<AddressEntity> detail(Long id) {
        AddressEntity entity = addressMapper.selectById(id);
        if (Objects.isNull(entity)) {
            return SingleResponse.buildFailure(ErrorCodeEnum.NO_DATA.getErrCode(), ErrorCodeEnum.NO_DATA.getErrDesc());
        }
        return SingleResponse.of(entity);
    }

    /**
     * 保存接口
     *
     * @param request
     * @return
     */
    public Response save(AddressDO request) {
        if (ObjectUtil.isNull(request.getId())) {
            AddressEntity target = buildTarget(request);
            if (!addressService.save(target)) {
                return Response.buildFailure(ErrorCodeEnum.SAVE_FAILED.getErrCode(), ErrorCodeEnum.SAVE_FAILED.getErrDesc());
            }
        } else {
            AddressEntity target = buildTarget(request);
            target.setId(request.getId());
            if (!addressService.updateById(target)) {
                return Response.buildFailure(ErrorCodeEnum.EDIT_FAILED.getErrCode(), ErrorCodeEnum.EDIT_FAILED.getErrDesc());
            }
        }
        return Response.buildSuccess();
    }

    /**
     * 创建target对象公共方法
     *
     * @param request
     * @return
     */
    private AddressEntity buildTarget(AddressDO request) {
        AddressEntity target = new AddressEntity();
        target.setReceiver(request.getReceiver());
        target.setDeliveryPhone(request.getDeliveryPhone());
        target.setAddress(request.getAddress());
        target.setUserId(request.getUserId());
        return target;
    }

    /**
     * 删除接口
     *
     * @param id
     * @return
     */
    public Response delete(Long id) {
        if (!addressService.removeById(id)) {
            return Response.buildFailure(ErrorCodeEnum.DELETE_FAILED.getErrCode(), ErrorCodeEnum.DELETE_FAILED.getErrDesc());
        }
        return Response.buildSuccess();
    }

}
