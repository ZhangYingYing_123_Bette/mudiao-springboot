package com.gigi.service.generated;

import com.gigi.entity.VideoEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 视频表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
public interface VideoService extends IService<VideoEntity> {

}
