package com.gigi.service.generated;

import com.gigi.entity.InheritorEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 传承人表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
public interface InheritorService extends IService<InheritorEntity> {

}
