package com.gigi.service.generated;

import com.gigi.entity.OrdersEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
public interface OrdersService extends IService<OrdersEntity> {

}
