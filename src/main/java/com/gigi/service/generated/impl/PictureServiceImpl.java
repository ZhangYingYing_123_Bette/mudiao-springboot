package com.gigi.service.generated.impl;

import com.gigi.entity.PictureEntity;
import com.gigi.mapper.generated.PictureMapper;
import com.gigi.service.generated.PictureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 图片表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-12
 */
@Service
public class PictureServiceImpl extends ServiceImpl<PictureMapper, PictureEntity> implements PictureService {

}
