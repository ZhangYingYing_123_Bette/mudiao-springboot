package com.gigi.service.generated.impl;

import com.gigi.entity.InheritorEntity;
import com.gigi.mapper.generated.InheritorMapper;
import com.gigi.service.generated.InheritorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 传承人表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
@Service
public class InheritorServiceImpl extends ServiceImpl<InheritorMapper, InheritorEntity> implements InheritorService {

}
