package com.gigi.service.generated.impl;

import com.gigi.entity.GoodsEntity;
import com.gigi.mapper.generated.GoodsMapper;
import com.gigi.service.generated.GoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, GoodsEntity> implements GoodsService {

}
