package com.gigi.service.generated.impl;

import com.gigi.entity.UserEntity;
import com.gigi.mapper.generated.UserMapper;
import com.gigi.service.generated.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

}
