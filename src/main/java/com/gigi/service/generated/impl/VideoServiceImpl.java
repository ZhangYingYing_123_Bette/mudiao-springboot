package com.gigi.service.generated.impl;

import com.gigi.entity.VideoEntity;
import com.gigi.mapper.generated.VideoMapper;
import com.gigi.service.generated.VideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 视频表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
@Service
public class VideoServiceImpl extends ServiceImpl<VideoMapper, VideoEntity> implements VideoService {

}
