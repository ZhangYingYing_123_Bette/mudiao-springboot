package com.gigi.service.generated.impl;

import com.gigi.entity.CommentEntity;
import com.gigi.mapper.generated.CommentMapper;
import com.gigi.service.generated.CommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论id 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, CommentEntity> implements CommentService {

}
