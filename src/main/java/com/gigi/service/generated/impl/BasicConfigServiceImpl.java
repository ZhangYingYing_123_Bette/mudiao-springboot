package com.gigi.service.generated.impl;

import com.gigi.entity.BasicConfigEntity;
import com.gigi.mapper.generated.BasicConfigMapper;
import com.gigi.service.generated.BasicConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 基础配置表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
@Service
public class BasicConfigServiceImpl extends ServiceImpl<BasicConfigMapper, BasicConfigEntity> implements BasicConfigService {

}
