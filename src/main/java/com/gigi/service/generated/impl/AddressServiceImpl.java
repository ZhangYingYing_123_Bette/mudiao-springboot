package com.gigi.service.generated.impl;

import com.gigi.entity.AddressEntity;
import com.gigi.mapper.generated.AddressMapper;
import com.gigi.service.generated.AddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收货地址表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
@Service
public class AddressServiceImpl extends ServiceImpl<AddressMapper, AddressEntity> implements AddressService {

}
