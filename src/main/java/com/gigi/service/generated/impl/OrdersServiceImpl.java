package com.gigi.service.generated.impl;

import com.gigi.entity.OrdersEntity;
import com.gigi.mapper.generated.OrdersMapper;
import com.gigi.service.generated.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, OrdersEntity> implements OrdersService {

}
