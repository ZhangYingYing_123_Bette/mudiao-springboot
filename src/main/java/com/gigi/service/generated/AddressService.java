package com.gigi.service.generated;

import com.gigi.entity.AddressEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收货地址表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
public interface AddressService extends IService<AddressEntity> {

}
