package com.gigi.service.generated;

import com.gigi.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
public interface UserService extends IService<UserEntity> {

}
