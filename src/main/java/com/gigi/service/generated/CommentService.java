package com.gigi.service.generated;

import com.gigi.entity.CommentEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论id 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
public interface CommentService extends IService<CommentEntity> {

}
