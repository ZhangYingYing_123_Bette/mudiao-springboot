package com.gigi.service.generated;

import com.gigi.entity.GoodsEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-04-07
 */
public interface GoodsService extends IService<GoodsEntity> {

}
