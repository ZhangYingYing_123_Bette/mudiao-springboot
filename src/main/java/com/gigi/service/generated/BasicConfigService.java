package com.gigi.service.generated;

import com.gigi.entity.BasicConfigEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 基础配置表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-17
 */
public interface BasicConfigService extends IService<BasicConfigEntity> {

}
