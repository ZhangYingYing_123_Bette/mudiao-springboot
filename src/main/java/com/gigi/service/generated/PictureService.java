package com.gigi.service.generated;

import com.gigi.entity.PictureEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 图片表 服务类
 * </p>
 *
 * @author AutoGenerator
 * @since 2024-02-12
 */
public interface PictureService extends IService<PictureEntity> {

}
