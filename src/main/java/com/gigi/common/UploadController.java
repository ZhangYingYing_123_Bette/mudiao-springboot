package com.gigi.common;

import com.alibaba.cola.dto.SingleResponse;
import com.gigi.service.manual.UploadService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 上传文件
 */
@RestController
@RequestMapping("upload")
public class UploadController {

    private final UploadService uploadPicService;

    public UploadController(UploadService uploadPicService) {
        this.uploadPicService = uploadPicService;
    }

    @PostMapping
    public SingleResponse<String> upload(@RequestParam("file") MultipartFile file) {
        return uploadPicService.upload(file);
    }


}
