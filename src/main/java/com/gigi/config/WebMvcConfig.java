package com.gigi.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 解决跨域问题：
 * 前后端分离，端口号不同，域名不一样
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    // 重写父类中跨域访问规则方法
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // 允许所有目录跨域访问
                .allowedOrigins("*") // 允许所有网站跨域访问
                .allowedMethods("*") // 允许所有方法跨域访问
                .allowCredentials(true); // 跨域访问时需要验证
    }

}
