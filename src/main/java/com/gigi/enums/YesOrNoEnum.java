package com.gigi.enums;

/**
 * 是否枚举
 */
public enum YesOrNoEnum {
    YES(1, "1", "YES"),
    NO(0, "0", "NO"),
    ;
    private Integer code;
    private String codeStr;
    private String desc;

    YesOrNoEnum(Integer code, String codeStr, String desc) {
        this.code = code;
        this.codeStr = codeStr;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getCodeStr() {
        return codeStr;
    }
}
