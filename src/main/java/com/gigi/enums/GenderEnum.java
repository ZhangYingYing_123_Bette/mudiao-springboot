package com.gigi.enums;

/**
 * 性别枚举
 */
public enum GenderEnum {
    MALE(0, "0", "男"),
    FEMALE(1, "1", "女"),
    GROUP(2, "2", "组合"),
    UN_KNOW(3, "3", "不明");

    private Integer code;
    private String codeStr;
    private String desc;

    GenderEnum(Integer code, String codeStr, String desc) {
        this.code = code;
        this.codeStr = codeStr;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getCodeStr() {
        return codeStr;
    }
}
