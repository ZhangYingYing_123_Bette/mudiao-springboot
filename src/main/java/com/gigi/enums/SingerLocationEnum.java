package com.gigi.enums;

import java.util.stream.Stream;

/**
 * 歌手地区枚举
 */
public enum SingerLocationEnum {
    INLAND(0, "0", "内地"),
    HONG_KONG_AND_TAIWAN(1, "1", "港台"),
    EUROPE_AND_AMERICA(2, "2", "欧美"),
    SOUTH_KOREA(3, "3", "韩国");

    private Integer code;
    private String codeStr;
    private String desc;

    SingerLocationEnum(Integer code, String codeStr, String desc) {
        this.code = code;
        this.codeStr = codeStr;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getCodeStr() {
        return codeStr;
    }

    public String getDesc() {
        return desc;
    }

    public static SingerLocationEnum getEnumByCode(final Integer code) {
        return Stream.of(SingerLocationEnum.values()).filter(item -> item.code.equals(code)).findFirst().orElse(null);
    }

    public static SingerLocationEnum getEnumByCodeStr(final String codeStr) {
        return Stream.of(SingerLocationEnum.values()).filter(item -> item.codeStr.equals(codeStr)).findFirst().orElse(null);
    }

}
