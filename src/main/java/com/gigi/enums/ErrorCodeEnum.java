package com.gigi.enums;

/**
 * 异常枚举
 */
public enum ErrorCodeEnum {
    NO_DATA("201", "未查询到数据"),
    SAVE_FAILED("202", "新增失败"),
    EDIT_FAILED("203", "更新失败"),
    DELETE_FAILED("204", "删除失败"),

    BIZ_EXCEPTION("500", "内部异常"),
    PARAM_ERROR("400", "参数错误"),

    FILE_EMPTY("2001", "上传文件不能为空"),
    UPLOAD_FILE_FAILED("2002", "上传文件失败"),

    ALREADY_REGISTER("4001", "该手机号已存在用户"),
    ACCOUNT_NOT_EXIST("4002", "用户不存在，请检查手机号密码是否正确"),


    ;


    private final String errCode;
    private final String errDesc;

    ErrorCodeEnum(String errCode, String errDesc) {
        this.errCode = errCode;
        this.errDesc = errDesc;
    }

    public String getErrCode() {
        return errCode;
    }

    public String getErrDesc() {
        return errDesc;
    }
}
