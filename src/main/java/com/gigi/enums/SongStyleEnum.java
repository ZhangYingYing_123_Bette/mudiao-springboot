package com.gigi.enums;

import java.util.stream.Stream;

/**
 * 歌曲流派枚举
 */
public enum SongStyleEnum {
    POP(0, "0", "流行"),
    ROCK_AND_ROLL(1, "1", "摇滚"),
    R_AND_B(2, "2", "R&B"),
    ELECTRONIC(3, "3", "电子"),
    RAP(4, "4", "说唱"),
    HIP_HOP(5, "5", "Hip Hop"),
    JAZZ(6, "6", "爵士");

    private Integer code;
    private String codeStr;
    private String desc;

    SongStyleEnum(Integer code, String codeStr, String desc) {
        this.code = code;
        this.codeStr = codeStr;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getCodeStr() {
        return codeStr;
    }

    public String getDesc() {
        return desc;
    }

    public static SongStyleEnum getEnumByCode(final Integer code) {
        return Stream.of(SongStyleEnum.values()).filter(item -> item.code.equals(code)).findFirst().orElse(null);
    }

    public static SongStyleEnum getEnumByCodeStr(final String codeStr) {
        return Stream.of(SongStyleEnum.values()).filter(item -> item.codeStr.equals(codeStr)).findFirst().orElse(null);
    }

}
